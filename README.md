# OER Handreichung

Kriterien für die OER Erstellung. Hinweise zu offenen Formaten: welche werden vom Projekt empfohlen?
Open Source Werkzeuge: welche werden vom Projekt empfohlen?


* Offene Formate: welche werden vom Projekt empfohlen? Unterschiedliche Medientypen beachten

* Open Source Werkzeuge: welche werden vom Projekt empfohlen? Unterschiedliche Medientypen beachten

* Didaktische Hinweise: wie erstelle ich gute Materialien? Problem, kann man pauschal nicht sagen, je nach Medientyp habe ich unterschiedliche Anforderungen, stattdessen Darlegung der unterschiedlichen Objekte (Informationsobjekte, Lernobjekte, Software, + Lehr-/Lernkonzepte) Checkliste für Beschreibungen

* Qualität: Qualitätsroutinen genau beschreiben